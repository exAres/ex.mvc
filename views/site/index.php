<div class='menu'>
<h2>Home page</h2>

<?php if(MVC::app()->user->role == 'guest'):?>
<div style="width: 90%;">
Please Login to use capabilities of this site.
</div>
<?php else: ?>
<h3>Menu:</h3>
<a href="/user/profile">Profile</a>
<br>
<a href="/user/messages">My messages</a>
    <?php if(MVC::app()->user->role == 'admin'):?>
        <h3>Administrating: </h3>
        <a href="/admin/users">Users</a>
        <br>
        <a href="/admin/messages">Messages</a>
        <br>
    <?php endif;?>
<?php endif; ?>
</div>