<?php

class UserController extends Controller
{

    private $header = '/layouts/header';
    private $footer = '/layouts/footer';

    function render($param,$_globals = NULL)
    {
        parent::render($param, $this->header, $this->footer, $_globals);
        exit();
    }

    public function actionLogin() {
        if (isset($_POST['name']) && isset($_POST['pass'])) {
            MVC::app()->Login($_POST['name'], $_POST['pass']);
        } elseif (isset($_POST['logout'])) {
            MVC::app()->Logout();
        }
        self::render('/site/index');
    }

    public function actionRegistration() {
        if (isset($_POST['name']) && isset($_POST['pass'])) {
            MVC::app()->Login($_POST['name'], $_POST['pass']);
        }
        self::render('/site/index');
    }

    public function actionProfile() {
        if(MVC::app()->user->role != 'guest'):
            $model = null;
            if(isset($_GET['id']) || isset($_GET['create'])){
                if(MVC::app()->user->role == 'admin'){
                    if(isset($_GET['create'])){
                        $model = new User();
                    }else{
                        $model = User::model()->findByPk($_GET['id']);
                    }
                }else{
                    Route::NotEnoughRights();
                }
            }
            self::render('/user/profile',array('model' => $model));
        else:
            Route::NotEnoughRights();
        endif;
    }

    public function actionWrite() {
        if(MVC::app()->user->role != 'guest'):
            $users = User::model()->findAllEnabled();
            $target = null;
            $messages = null;
            if(isset($_GET['id'])){
                foreach ($users as $user) {
                    if($_GET['id'] == $user->id){
                        $target = $user;
                    }
                }
                if($target != null){
                    $messages = Messages::model()->findAllFromUser($target->id);
                    Messages::model()->MarkAsReaded($target->id);
                }else{
                    Route::ErrorPage404();
                }
            }
            self::render('/user/write',array('users' => $users,'target' => $target,'messages' => $messages));
        else:
            Route::NotEnoughRights();
        endif;
    }

    public function actionMessages() {
        if(MVC::app()->user->role != 'guest'):
            $filter = null;
            if(isset($_GET['filter'])){
                $filter = $_GET['filter'];
            }
            $messages = Messages::model()->findAllByFilter($filter);
            self::render('/user/messages',array('messages' => $messages));
        else:
            Route::NotEnoughRights();
        endif;
    }

    public function actionSendmessage(){
        if(isset($_POST['Message']) && $_POST['Message']['text'] != '' && $_POST['Message']['target_id'] != 0){
            $message = new Messages();
            $message->target_id = $_POST['Message']['target_id'];
            $message->sender_id = MVC::app()->user->id;
            $message->text = $_POST['Message']['text'];
            $message->status = 'new';
            $message->save();
            self::redirect('/user/write/id/'.$_POST['Message']['target_id']);
        }else{
            Route::ErrorPage404();
        }
    }

    public function actionEditprofile(){
        if(isset($_POST['User'])){
            if($_POST['User']['id'] == MVC::app()->user->id || MVC::app()->user->role == 'admin'){
                if($_POST['User']['id'] == NULL){
                    $model = new User();
                }else{
                    $model = User::model()->findByPk($_POST['User']['id']);
                }
                $model->name = $_POST['User']['name'];
                $model->login = $_POST['User']['login'];
                if($_POST['User']['password'] != ''){
                    $model->password = $_POST['User']['password'];
                }
                if(MVC::app()->user->role == 'admin'){
                    if(isset($_POST['User']['status'])){
                        $model->status = $_POST['User']['status'];
                    }
                    if(isset($_POST['User']['role'])){
                        $model->role = $_POST['User']['role'];
                    }
                }
                $model->save();
                if($_POST['User']['id'] == MVC::app()->user->id){
                    self::redirect('/user/profile/id/'.$_POST['User']['id']);
                }else{
                    self::redirect('/admin/users/');
                }
            }else{
                Route::NotEnoughRights();
            }
        }
    }
}

?>