<?php

class SiteController extends Controller
{

    private $header = '/layouts/header';
    private $footer = '/layouts/footer';

    function render($param,$_globals = NULL)
    {
        parent::render($param, $this->header, $this->footer, $_globals);
        exit();
    }

    public function actionIndex()
    {
        self::render('/site/index');
    }
    

}

?>