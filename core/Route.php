<?php

class Route {
    
   public static $_CURRENT_URL;

    static function start() {
        $controller_name = 'site';
        $action_name = 'index';
        $parse = 1;
        $URL = $_SERVER['REQUEST_URI'];
        if (strpos($URL, '?')) {
            $newurl = '';
            $var = explode('?', $URL);
            parse_str($var[1], $str);
            foreach ($str as $key => $value) {
                if ($key !== 'submit') {
                    $newurl = $newurl . '/' . $key . '/' . $value;
                }
            }
            header('Location:' . $var[0] . $newurl);
        } else {
            $routes = explode('/', $URL);
            if($routes[0] == 'localhost' || $routes[0] == __SITE_PATH || $routes[0] == __BASE_PATH) $parse++;
            if (!empty($routes[$parse])) {
                $controller_name = $routes[$parse];
                $parse++;
            }
            if (!empty($routes[$parse])) {
                $action = explode('?', $routes[$parse]);
                $parse++;
                $action_name = $action[0];
                if (!empty($routes[$parse])) {
                    for ($i = $parse; !empty($routes[$i]); $i = $i + 2) {
                        if (!empty($routes[$i + 1])) {
                            $_GET[$routes[$i]] = $routes[$i + 1];
                        }
                    }
                }
            }
        }
        $controller_base_name = $controller_name;
        $controller_name = strtoupper(substr($controller_name, 0, 1)) . strtolower(substr($controller_name, 1));
        $controller_name = $controller_name . 'Controller';
        $controller_file = $controller_name . '.php';
        $controller_path = __SITE_PATH . "/controllers/" . $controller_file;
        if (file_exists($controller_path)) {
            include __SITE_PATH . "/controllers/" . $controller_file;
        } else {

            Route::ErrorPage404();
        }
        $controller = new $controller_name();
        $action_base_name = $action_name;
        $action_name = strtoupper(substr($action_name, 0, 1)) . strtolower(substr($action_name, 1));
        $action = 'action' . $action_name;

        if (method_exists($controller, $action)) {
            $GLOBALS['_PARSED_URL'] = '/'.$controller_base_name.'/'.$action_base_name;
            $controller->$action();
        } else {
            Route::ErrorPage404();
        }
    }

    function ErrorPage404() {
        $host = 'http://' . __BASE_PATH . '/views/404.php';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host);
    }
    
        function NotEnoughRights() {
        $host = 'http://' . __BASE_PATH . '/views/rights.php';
        header('HTTP/1.1 Not Enough Rights');
        header("Status: Not Enough Rights");
        header('Location:' . $host);
    }

}