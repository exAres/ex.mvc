<?php

class MVC {
    public $user;

    private static $_instance;

    private function __construct() {
        session_start();
        if (isset($_SESSION['user'])) {
            $this->user = $_SESSION['user'];
        } else {
            $this->user = new User();
            $this->user->id = 0;
            $this->user->name = 'guest';
            $this->user->login = 'guest';
            $this->user->password = '';
            $this->user->role = 'guest';
        }
    }

    private function __clone() {
    }
    
    public function Login($name,$pass){
        $model = User::model()->find("`login`='".$name."' AND `password`='".$pass."'");
        if(!empty($model)){
            $this->user = $_SESSION['user'] = $model;
            return true;
        }
        else return false;
    }
    
    public function Logout(){
        session_destroy();
        $this->user->id = 0;
        $this->user->name = 'guest';
        $this->user->login = 'guest';
        $this->user->password = '';
        $this->user->role = 'guest';
    }

    public static function app() {
        if (self::$_instance) {
            self::$_instance;
        } else {
            self::$_instance = new MVC();
        }
        return self::$_instance;
    }

}