<?php

class DefaultModel
{

    public static $_models = array();

    public function __construct()
    {
        $this->connection = DBC::getInstance();
    }

    public function query($sql)
    {
        return mysql_query($sql, $this->connection);
    }

    public function select($sql)
    {
        return $this->toAssoc($this->query($sql));
    }

    public function toAssoc($q)
    {
        $array = array();
        if ($q) {
            while ($row = mysql_fetch_assoc($q)) {
                $array[] = $row;
            }
            mysql_free_result($q);
        }
        return $array;
    }

    public function insert($table, array $data)
    {
        if (!$data || !is_array($data)) {
            throw new Exception('Data should be an array');
        }
        $sql = "INSERT INTO `$table` (`" . implode("`,`", array_keys($data)) . "`) VALUES ('" . implode(
                "','",
                array_values($data)
            ) . "')";
        $this->query($sql);
    }

    public function update($table,array $data)
    {
        if (!$data || !is_array($data)) {
            throw new Exception('Data should be an array');
        }
        $val = '';
        $count = 0;
        foreach ($data as $key => $value) {
            if($count != 0){
                $val = $val.',';
            }
            $val .= $key.'="'.$value.'"';
            $count++;
        }
        $sql = "update `$table` set ".$val.' where id = '.$this->id;
        $this->query($sql);
    }

    public function autoInsert($tablename)
    {
        unset($_POST['Submit']);
        foreach ($_POST as $key => $value) {
            $_POST[$key] = str_replace(array('\'', '>', '<'), '', $value);
        }
        $this->insert($tablename, $_POST);
    }

    public function delete()
    {
        $sql = "delete from " . $this->table_name . " where id = " . $this->id;
        $q = mysql_query($sql, $this->connection);
        return $q;
    }

    public function disconnect()
    {
        return mysql_close($this->connection);
    }

    public function arrayToObj(array $array, $class = 'stdClass', $parent = 1)
    {
        if (!empty($array)) {
            $object = new $class;
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    $value = $this->arrayToObj($value, $class, $parent + 1);
                }
                if (count($array) == 1) {
                    $object = $value;
                } else {
                    $object->{$key} = $value;
                }
            }
            return $object;
        } else {
            return $array;
        }
    }

    public function arrayToObjs(array $array, $class = 'stdClass', $parent = 1)
    {
        if (!empty($array)) {

            if ($parent == 1) {
                $object = Array();
            } else {
                $object = new $class;
            }
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    $value = $this->arrayToObj($value, $class, $parent + 1);
                }
                if ($parent == 1) {
                    $object[] = $value;
                } else {
                    $object->{$key} = $value;
                }
            }
            return $object;
        } else {
            return $array;
        }
    }

    public function scriptFind($script, $class = 'stdClass')
    {
        return $this->arrayToObj($this->select($script), $class);
    }

    public function scriptFindAll($script, $class = 'stdClass')
    {
        return $this->arrayToObjs($this->select($script), get_class($this));
    }

    public function find($where = '', $select = '*', $limit = '')
    {
        return $this->arrayToObj(
            $this->select(
                'SELECT ' . $select . ' FROM ' . strtolower(get_class($this)) . ' WHERE ' . $where . ' ' . $limit
            ),
            get_class($this)
        );
    }

    public function findAll()
    {
        return $this->arrayToObjs($this->select('SELECT * FROM ' . strtolower(get_class($this))));
    }

    public function findByPk($pk)
    {
        return $this->arrayToObj(
            $this->select('SELECT * FROM ' . strtolower(get_class($this)) . ' WHERE id=' . $pk),
            get_class($this)
        );
    }


    public static function model($className = __CLASS__)
    {
        if (isset(self::$_models[$className])) {
            return self::$_models[$className];
        } else {
            $model = self::$_models[$className] = new $className(null);
            return $model;
        }
    }

}
