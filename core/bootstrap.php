<?php 

// -- Autoloader from /models and /controllers
if(!function_exists('classAutoLoader')){
    function classAutoLoader($class){
    //$class=strtolower($class);
    $classFile=__SITE_PATH.'/models/'.$class.'.php';
       if(is_file($classFile)&&!class_exists($class)) include $classFile;
       else {
           $classFile=__SITE_PATH.'/controllers/'.$class.'.php';
           if(is_file($classFile)&&!class_exists($class)) include $classFile;
       }
    }
}
spl_autoload_register('classAutoLoader');
// -- end of Autoloader
?>